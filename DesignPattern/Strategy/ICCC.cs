﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    public class ICCC : Imposto
    {
        public double Calcula(Double valor)
        {
            double valorCalculado;

            if(valor < 1000)
            {
                valorCalculado = valor * 0.05;
            }
            else if(valor <= 3000)
            {
                valorCalculado = valor * 0.07;
            }
            else
            {
                valorCalculado = (valor * 0.08) + 30;
            }

            return valorCalculado;
        }
    }
}
