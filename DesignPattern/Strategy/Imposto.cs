﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    public interface Imposto
    {
        double Calcula(double valor);
    }
}
