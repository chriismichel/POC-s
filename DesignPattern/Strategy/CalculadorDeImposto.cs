﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    public class CalculadorDeImposto
    {
        public double Calcula(Orcamento orcamento, Imposto imposto)
        {
            return imposto.Calcula(orcamento.Valor);
        }
    }
}
