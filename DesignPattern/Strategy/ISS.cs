﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    class ISS : Imposto
    {
        public double Calcula(double valor)
        {
            var valorCalculado = valor * 0.06;
            return valorCalculado;
        }
    }
}
