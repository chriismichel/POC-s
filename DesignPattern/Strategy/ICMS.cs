﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
    class ICMS : Imposto
    {
        public double Calcula(double valor) {
            var valorCalculado = (valor * 0.05) + 50;
            return valorCalculado;
        }
    }
}
