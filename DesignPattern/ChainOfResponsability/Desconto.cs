﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChainOfResponsability
{
    public interface Desconto
    {
        double Desconta(Orcamento orcamento);
        Desconto Proximo { get; set; }
    }
}
