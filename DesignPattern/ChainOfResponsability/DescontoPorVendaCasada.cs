﻿using System;

namespace ChainOfResponsability
{
    internal class DescontoPorVendaCasada : Desconto
    {
        public Desconto Proximo { get; set; }

        public double Desconta(Orcamento orcamento)
        {
            bool caneta = Existe("caneta", orcamento);
            bool lapis = Existe("lapis", orcamento);

            if(caneta || lapis)
            {
                double novoValor = orcamento.Valor * 0.05;
                return novoValor;
            }

            return Proximo.Desconta(orcamento);
        }

        private bool Existe(String nome, Orcamento orcamento)
        {
            foreach (Item item in orcamento.Itens)
            {
                if (item.Nome.Equals(nome))
                    return true;
            }
            return false;
        }
    }
}